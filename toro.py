import cv2
import numpy as np
import sys

def nothing(x):
    pass

def swits(estado):
    if estado:
        estado = False
    else:
        estado = True

def hayRojo(area,img):
    a = len(img)
    l = len(img[0])
    pedazoImg = (a/3 * l/4)
    if area > pedazoImg:
        return True
    return False

def encontrarBlob(contours):
    areaMayor = 0
    contMayor = contours[0]
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if(area > areaMayor):
            areaMayor = area
            contMayor = cnt

    maxX = maxY = 0
    minX = minY = 100000
    cX = cY = 0
    for i in contMayor:
        x = i[0][0]
        y = i[0][1]
        maxX = max(maxX, x)
        minX = min(minX, x)
        maxY = max(maxY, y)
        minY = min(minY, y)
    maxX += 50
    minX -= 50
    minY -= 50
    maxY += 50 

    M = cv2.moments(contMayor)
    if(M['m00']!=0):
        cX = int(M['m10']/M['m00'])
        cY = int(M['m01']/M['m00'])

    return minY,maxY,minX,maxX,cX,cY

###SETUP
camara = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_PLAIN
k3 = (3,3)
k5 = (5,5)
k9 = (9,9)



while(True):

    ###TOMAR FOTO
    _,frame = camara.read()
    mos = frame #Frame para presentacion
    area = 0
    ###RANGOS########################
    rInf =  np.array([146,139,158])
    rSup =  np.array([179,255,255])
    #################################


    ###FILTRO
    bilBlur = cv2.bilateralFilter(frame,7,75,75)
    hsv = cv2.cvtColor(bilBlur, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv,rInf,rSup)
    mk1 = cv2.inRange(hsv,rInf,rSup) #NOSE POR QUE NO LA PUEDO COPIAR

    _,contours,_ = cv2.findContours(mk1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cX = cY = 0
    if len(contours) != 0: #Si existen blobs encontrar el area del mayor
        minY,maxY,minX,maxX,cX,cY = encontrarBlob(contours)
        area = (maxY-minY)*(maxX-minX)
        imgFin = mask[minY:minY+int(1.61*(maxY-minY)),minX:maxX]
        imgFin = cv2.morphologyEx(imgFin, cv2.MORPH_CLOSE, k3)

    ###PRESENTACION
    cv2.putText(mos,("Hay rojo? "+str(hayRojo(area,frame))),(20,30),font,1,(0,0,0),1,cv2.LINE_AA)
    if(hayRojo(area,frame)):
        cv2.putText(mos,("Coordenadas"+str(cX)+" "+str(cY)),(20,40),font,1,(0,0,0),1,cv2.LINE_AA)
    #   cv2.rectangle(mos,(minX,minY),(maxX,maxY),(0,255,0))


    cv2.imshow('frame',mos)
    cv2.imshow('mask',mask)
   
    ###CONTROLES
    k = cv2.waitKey(30) & 0xff#Bitwise And: Abs #Escuchar Teclado    

    if k == ord('z'):
        swits(tomandoFrames)  
    elif k == 27:
        break

    '''
    if k == ord(''):
        cv2.imshow('ventana',img) 
    '''

camara.release()
cv2.destroyAllWindows()
