import cv2
import numpy as np

def encontrarMano(frame,nueva):
        _,contours,_ = cv2.findContours(frame,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) != 0:
                areaMayor = 0
                contMayor = contours[0]
                for cnt in contours:
                    area = cv2.contourArea(cnt)
                    if(area > areaMayor):
                        areaMayor = area
                        contMayor = cnt
                maxX = maxY = 0
                minX = minY = 100000
                for i in contMayor:
                    x = i[0][0]
                    y = i[0][1]
                    maxX = max(maxX, x)
                    minX = min(minX, x)
                    maxY = max(maxY, y)
                    minY = min(minY, y)
                maxX += 50
                minX -= 50
                minY -= 50
                maxY = minY + ( maxX - minX )
                nueva = cv2.cvtColor(nueva,cv2.COLOR_RGB2GRAY)
                _,nueva = cv2.threshold(nueva,70,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
                return nueva[minY:maxY,minX:maxX]
        return nueva
camara = cv2.VideoCapture(0)
restador = cv2.createBackgroundSubtractorKNN(500,800.0,False)
cont = 0

while(True):
    _,frame = camara.read()
    
    cv2.imshow("Inicio",frame)

    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    can = cv2.Canny(gray,100,150)
    blur = cv2.blur(gray,(4,4))
    
    #source,puntomedio,valormaximo,THRESH_
    _,th1 = cv2.threshold(blur,130,255,cv2.THRESH_BINARY_INV)

    cv2.imshow("TH1",th1)
    
    _,th3 = cv2.threshold(blur,130,255,cv2.THRESH_BINARY_INV)
    im2, contours, hierarchy = cv2.findContours(th1,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

    cv2.imshow("IM2",im2)
    
    cv2.drawContours(th1,contours,-1,(255,255,255),2)
    kernel = (15,15)
    img = encontrarMano(th3,frame)
    closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

    cv2.imshow("Video",frame) #Mostrar la imagen loop

    k = cv2.waitKey(30) & 0xff #Bitwise And: Abs
    if img.size != 0:
        cv2.imshow("opne",closing)
        if k == ord('z'):
            cv2.imwrite(('img'+str(cont)+'.png'),closing)
            cont += 1
    

    if k == ord('m'):
        cv2.imshow('mask',mask)
    if k == ord('r'):
        restador = cv2.createBackgroundSubtractorKNN(500,800.0,False)
    if k == ord('v'):
        cv2.imshow('res',res)
    
        
    if k == ord('s'):
        cv2.imwrite('img.png',frame)
    if k == 27:
        break

camara.release()
cv2.destroyAllWindows()
