import cv2
import numpy as np
import sys

class ImagenPre:

    def __init__(self,ruta):
        self.img = cv2.imread(ruta)
        self.nueva = self.img

    def __init__(self):
        self.capturar()
        self.nueva = self.img

    def mostrar(self,nombreVentana):
        cv2.imshow(nombreVentana,self.img)
        k = cv2.waitKey(0)
        if k == 27:
            self.cerrarVentana(nombreVentana)

    def escribir(self,ruta):
        cv2.imwrite(ruta,self.img)

    def procesar(self):
        self.cortarBrazo()
        self.escalaGrises()
        self.blur()
        self.binarizar()
        self.encontrarMano()
        self.cortar()

    def mostrarProceso(self):
        self.mostrar('Imagen Captada')
        self.cortarBrazo()
        self.mostrar('Cortar Exceso Brazo')
        self.escalaGrises()
        self.mostrar('Escala de Grises')
        self.blur()
        self.mostrar('Gaussian Blur')
        self.binarizar()
        self.mostrar('Binaria')
        self.encontrarMano()
        self.mostrar('Mano Encontrada')
        self.cortar()
        self.mostrar('Mini')

    def cortarBrazo(self):
        self.img = self.img[:-100,:]

    def escalaGrises(self):
        self.img = cv2.cvtColor(self.img,cv2.COLOR_RGB2GRAY)

    def blur(self):
        self.img = cv2.GaussianBlur(self.img,(9,9),0)

    def binarizar(self):
        _,self.img = cv2.threshold(self.img,70,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

    def encontrarMano(self):
        _,contours,_ = cv2.findContours(self.img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        areaMayor = 0
        contMayor = contours[0]
        for cnt in contours:
            area = cv2.contourArea(cnt)
            if(area > areaMayor):
                areaMayor = area
                contMayor = cnt
        maxX = maxY = 0
        minX = minY = 100000
        for i in contMayor:
            x = i[0][0]
            y = i[0][1]
            maxX = max(maxX, x)
            minX = min(minX, x)
            maxY = max(maxY, y)
            minY = min(minY, y)
        maxX += 50
        minX -= 50
        minY -= 50
        maxY = minY + ( maxX - minX )
        self.nueva = cv2.cvtColor(self.nueva,cv2.COLOR_RGB2GRAY)
        _,self.nueva = cv2.threshold(self.nueva,70,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        self.img = self.nueva[minY:maxY,minX:maxX]

    def cortar(self):
        self.img = cv2.resize(self.img,(20,20))

    def capturar(self):
        camara = cv2.VideoCapture(0)
        while(True):
            _,frame = camara.read()
            cv2.imshow('Camara',frame)
            k = cv2.waitKey(20)
            if k == ord('s'):
                self.img = frame
                break
        camara.release()
        cv2.destroyWindow('Camara')

    def cerrarVentana(self,nom):
        cv2.destroyWindow(nom)
        sys.exit()
