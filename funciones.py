import cv2
import numpy as np
import sys


def mostrar(frame,nombreVentana):
    cv2.imshow(nombreVentana,frame)
    k = cv2.waitKey(0)
    if k == 27:
        cerrarVentana(nombreVentana)

def escribir(self,ruta):
    cv2.imwrite(ruta,self.img)

def procesar(frame):
    nueva = frame
    frame = cortarBrazo(frame)
    frame = escalaGrises(frame)
    frame = blur(frame)
    frame = binarizar(frame)
    frame = encontrarMano(frame,nueva)
    return frame

def cortarBrazo(frame):
    frame = frame[:-100,:]
    return frame

def escalaGrises(frame):
    frame = cv2.cvtColor(frame,cv2.COLOR_RGB2GRAY)
    return frame

def blur(frame):
    frame = cv2.GaussianBlur(frame,(9,9),0)
    return frame

def binarizar(frame):
    f1 = frame
    _,frame = cv2.threshold(f1,70,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    return frame

def encontrarMano(frame,nueva):
    f1 = frame
    _,contours,_ = cv2.findContours(f1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    areaMayor = 0
    contMayor = contours[0]
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if(area > areaMayor):
            areaMayor = area
            contMayor = cnt
    maxX = maxY = 0
    minX = minY = 100000
    for i in contMayor:
        x = i[0][0]
        y = i[0][1]
        maxX = max(maxX, x)
        minX = min(minX, x)
        maxY = max(maxY, y)
        minY = min(minY, y)
    maxX += 50
    minX -= 50
    minY -= 50
    maxY = minY + ( maxX - minX )
    nueva = cv2.cvtColor(nueva,cv2.COLOR_RGB2GRAY)
    n1 = nueva
    _,nueva = cv2.threshold(n1,70,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    frame = nueva[minY:maxY,minX:maxX]
    return frame

def cortar(self):
    self.img = cv2.resize(self.img,(20,20))

def capturar():
    tomandoFrames = False
    cont = 0
    font = cv2.FONT_HERSHEY_COMPLEX
    camara = cv2.VideoCapture(0)
    while(True):
        _,frame = camara.read()
        mos = frame
        cv2.putText(mos,("Tomar Frames: "+str(tomandoFrames)),(20,30),font,1,(255,255,255),2,cv2.LINE_AA)
        cv2.imshow('Camara',mos)
        k = cv2.waitKey(30) & 0xff
        if k == ord('z'):
            if tomandoFrames:
                tomandoFrames = False
            else:
                tomandoFrames = True
        if k == ord('s'):
            break
        if tomandoFrames:
            cv2.imwrite(('a/img'+str(cont)+'.png'),procesar(frame))
            cont+=1
    camara.release()
    cv2.destroyWindow('Camara')
    return frame

def cerrarVentana(nom):
    cv2.destroyWindow(nom)
    sys.exit()
