import cv2
import numpy as np

camara = cv2.VideoCapture(0)

_,frame = camara.read()
b,g,r = cv2.split(frame)

while(True):
    _,frame = camara.read()


    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    lRed = np.array([5,30, 0])
    uRed = np.array([255,255,255])

    mask = cv2.inRange(hsv,lRed,uRed)

    res = cv2.bitwise_and(frame,frame,mask = mask)
    cv2.imshow("Video",frame) #Mostrar la imagen loop

    k = cv2.waitKey(30) & 0xff #Bitwise And: Abs

    if k == ord('m'):
        cv2.imshow('mask',mask)
    if k == ord('p'):
        cv2.imshow('res',res)
        print frame
    if k == ord('b'):
        b = cv2.split(frame)[0]
    if k == ord('g'):
        g = cv2.split(frame)[1]
    if k == ord('r'):
        r = cv2.split(frame)[2]
    if k == ord('v'):
        frame = cv2.merge((b,g,r))
        cv2.imshow("Previo",frame)
        
    if k == ord('s'):
        frame = cv2.merge((b,g,r))
        cv2.imwrite('img.png',frame)
    if k == 27:
        break

camara.release()
cv2.destroyAllWindows()
