


import cv2
import numpy as np
import sys

def nothing(x):
    pass

camara = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_PLAIN

###CREAR C
cv2.namedWindow('control')
cv2.createTrackbar('HI', 'control', 0, 179, nothing)
cv2.createTrackbar('HS', 'control', 20, 179, nothing)
cv2.createTrackbar('SI', 'control', 0, 255, nothing)
cv2.createTrackbar('SS', 'control', 50, 255, nothing)
cv2.createTrackbar('VI', 'control', 0, 255, nothing)
cv2.createTrackbar('VS', 'control', 50, 255, nothing)

gmi = np.zeros((300,512,3), np.uint8)

while(True):
    ##TOMAR FOTO##
    _,frame = camara.read()



    ######RANGOS#####################
    hi = cv2.getTrackbarPos('HI','control')
    si = cv2.getTrackbarPos('SI','control')
    vi = cv2.getTrackbarPos('VI','control')

    gmi[:] = [hi,si,vi]
    cv2.putText(gmi,str(cv2.cvtColor(gmi,cv2.COLOR_BGR2HSV)),(20,40),font,1,(0,0,0),1,cv2.LINE_AA)
    cv2.imshow('control',gmi)
    
    k = cv2.waitKey(30) & 0xff#Bitwise And: Abs #Capturar Tecla

    if k == 27:
        break

    '''
    if k == ord(''):
        cv2.imshow('ventana',img) 
    '''

camara.release()
cv2.destroyAllWindows()
