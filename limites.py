import cv2
import numpy as np
import sys


def promediar(cuadrante):
    r=g=b=0
    for i in range(len(cuadrante)):
        for j in range(len(cuadrante[i])):
            r += cuadrante[i][j][0]
            g += cuadrante[i][j][1]
            b += cuadrante[i][j][2]
    x = len(cuadrante[0])
    y = len(cuadrante)
    r /= (x*y)
    g /= (x*y)
    b /= (x*y)
    return r,g,b




camara = cv2.VideoCapture(0)


#Ciclo de video
while(True):
    _,frame = camara.read()
    cv2.imshow('Video',frame)
    k = cv2.waitKey(30) & 0xff
    if k == ord('s'):
        break
    
camara.release()
cv2.destroyWindow('Video')


#len(frame) filas Y : len(frame[0]) columnas X
y = len(frame)
x = len(frame[0])

c1 = frame[0:int(y/3),0:int(x/3)]
c2 = frame[0:int(y/3),int(x/3):(2*int(x/3))]
c3 = frame[0:int(y/3),(2*int(x/3)):]
c4 = frame[int(y/3):(2*int(y/3)),0:int(x/3)]
c5 = frame[int(y/3):(2*int(y/3)),int(x/3):(2*int(x/3))]
c6 = frame[int(y/3):(2*int(y/3)),(2*int(x/3)):]
c7 = frame[(2*int(x/3)):,0:int(x/3)]
c8 = frame[(2*int(x/3)):,int(x/3):(2*int(x/3))]
c9 = frame[(2*int(x/3)):,(2*int(x/3)):]




c = [[promediar(c1),promediar(c2),promediar(c3)],[promediar(c4),promediar(c5),promediar(c6)],[promediar(c7),promediar(c8),promediar(c9)]]
