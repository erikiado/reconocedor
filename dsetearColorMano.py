import cv2
import numpy as np
import sys

def nothing(x):
    pass

def swits(estado):
    if estado:
        estado = False
    else:
        estado = True


def encontrarMano(contours):
    areaMayor = 0
    contMayor = contours[0]
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if(area > areaMayor):
            areaMayor = area
            contMayor = cnt

    maxX = maxY = 0
    minX = minY = 100000
    for i in contMayor:
        x = i[0][0]
        y = i[0][1]
        maxX = max(maxX, x)
        minX = min(minX, x)
        maxY = max(maxY, y)
        minY = min(minY, y)
    maxX += 50
    minX -= 50
    minY -= 50
    maxY += 50 
    
    return minY,maxY,minX,maxX

###SETUP
camara = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_PLAIN
_,imgFin = camara.read()
tomandoFrames = False
k3 = (3,3)
k5 = (5,5)
k9 = (9,9)

###INPUT
path = "dset/"+ sys.argv[1] +"/"
cont = 300 * int(sys.argv[2])



while(True):

    ###TOMAR FOTO
    _,frame = camara.read()
    mos = frame #Frame para presentacion
    pathImg = path + str(cont) + ".png"


    ###RANGOS########################
    pInf = np.array([0,20,10])
    pSup = np.array([30,200,255])

    rInf =  np.array([170,20,10])
    rSup =  np.array([179,230,255])
    #################################


    ###FILTRO
    bilBlur = cv2.bilateralFilter(frame,7,75,75)
    hsv = cv2.cvtColor(bilBlur, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv,pInf,pSup) + cv2.inRange(hsv,rInf,rSup)
    mk1 = cv2.inRange(hsv,pInf,pSup) + cv2.inRange(hsv,rInf,rSup) #NOSE POR QUE NO LA PUEDO COPIAR

    _,contours,_ = cv2.findContours(mk1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cv2.imshow('f',mask)

    if len(contours) != 0: #Si existen blobs encontrar el area del mayor
        minY,maxY,minX,maxX = encontrarMano(contours)
        imgFin = mask[minY:minY+int(1.61*(maxY-minY)),minX:maxX]
        imgFin = cv2.morphologyEx(imgFin, cv2.MORPH_CLOSE, k3)

    ###PRESENTACION
    cv2.putText(mos,("Tomar Frames: "+str(tomandoFrames)),(20,30),font,1,(0,0,0),1,cv2.LINE_AA)
    cv2.putText(mos,("Path: ./"+pathImg),(20,40),font,1,(0,0,0),1,cv2.LINE_AA)
    #   cv2.rectangle(mos,(minX,minY),(maxX,maxY),(0,255,0))


    cv2.imshow('frame',mos)
    if imgFin != None:
        if imgFin.size != 0:
            cv2.imshow('x',imgFin)
            if tomandoFrames:
                #cv2.imwrite(pathImg,imgFin)
                cont+=1

    ###CONTROLES
    k = cv2.waitKey(30) & 0xff#Bitwise And: Abs #Escuchar Teclado    

    if k == ord('z'):
        swits(tomandoFrames)  
    elif k == 27 or cont >= ((int(sys.argv[2])*300)+300):
        break

    '''
    if k == ord(''):
        cv2.imshow('ventana',img) 
    '''

camara.release()
cv2.destroyAllWindows()
