import cv2
import numpy as np

camara = cv2.VideoCapture(0)

_,frame = camara.read()

while(True):
    _,frame = camara.read()

    h = np.zeros((300,256,3))
    bins = np.arange(256).reshape(256,1)
    color = [ (255,0,0),(0,255,0),(0,0,255) ]
    for ch, col in enumerate(color):
    	hist_item = cv2.calcHist([frame],[ch],None,[256],[0,256])
    	cv2.normalize(hist_item,hist_item,0,255,cv2.NORM_MINMAX)
    	hist=np.int32(np.around(hist_item))
    	pts = np.column_stack((bins,hist))
    	cv2.polylines(h,[pts],False,col)

    h=np.flipud(h)
    cv2.imshow('col',h)

    cv2.imshow("Video",frame) #Mostrar la imagen loop

    k = cv2.waitKey(30) & 0xff #Bitwise And: Abs

    if k == ord('v'):
        cv2.imshow("Previo",frame)
    if k == 27:
        break

camara.release()
cv2.destroyAllWindows()

